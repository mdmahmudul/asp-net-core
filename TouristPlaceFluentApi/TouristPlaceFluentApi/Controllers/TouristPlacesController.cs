﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TouristPlaceFluentApi.Data;
using TouristPlaceFluentApi.Models;

namespace TouristPlaceFluentApi.Controllers
{
    [Authorize]
    public class TouristPlacesController : Controller
    {
        private readonly TouristPlaceContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment;
        public TouristPlacesController(TouristPlaceContext context, IWebHostEnvironment HostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = HostingEnvironment;
        }

        public async Task<IActionResult> Index(string SearchValue, Boolean? IsAscending)
        {
            if (SearchValue == null)
            {
                SearchValue = "";
                HttpContext.Session.SetString("searchValue", SearchValue);
            }
            else
            {
                HttpContext.Session.SetString("searchValue", SearchValue);
            }

            if (IsAscending != null)
            {
                IsAscending = !IsAscending;
                HttpContext.Session.SetString("sort", IsAscending.ToString());
            }
            else if (IsAscending == null && HttpContext.Session.GetInt32("sort") != null)
            {
                IsAscending = Convert.ToBoolean(HttpContext.Session.GetString("sort"));
            }
            else
            {
                IsAscending = false;
                HttpContext.Session.SetString("sort", IsAscending.ToString());
            }

            ViewData["sortValue"] = IsAscending;
            ViewData["searchValue"] = SearchValue;

            if (IsAscending.Value)
            {
                return View("Index", await _context.TouristPlaces.Where(m => m.Name.Contains(SearchValue)).OrderByDescending(m => m.Rating).ToListAsync());
            }
            else
            {
                return View("Index", await _context.TouristPlaces.Where(m => m.Name.Contains(SearchValue)).OrderBy(m => m.Rating).ToListAsync());
            }
            //Without Lazy Loading
            /*
             if (IsAscending.Value)
            {
                return View("Index", await _context.TouristPlaces.Where(m => m.Name.Contains(SearchValue)).Include(m=>m.Type).OrderByDescending(m => m.Rating).ToListAsync());
            }
            else
            {
                return View("Index", await _context.TouristPlaces.Where(m => m.Name.Contains(SearchValue)).Include(m=>m.Type).OrderBy(m => m.Rating).ToListAsync());
            }
             */
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var touristPlace = await _context.TouristPlaces
                .FirstOrDefaultAsync(m => m.Id == id);
            //Without Lazy Loading
            /*var touristPlace = await _context.TouristPlaces.Include(m=>m.Type)
                .FirstOrDefaultAsync(m => m.Id == id);*/
            if (touristPlace == null)
            {
                return NotFound();
            }

            return View(touristPlace);
        }

        public IActionResult Create()
        {
            ViewBag.Types = _context.PlaceTypes.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(IFormFile Image, [Bind("Id, Name, Image, Rating, Address, TypeId")] TouristPlace touristPlace)
        {
            if (ModelState.IsValid)
            {
                if (Image == null)
                    return View(touristPlace);
                touristPlace.ImageUrl = SaveImageReturnUniqueName(Image);
                _context.Add(touristPlace);

                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { searchValue = HttpContext.Session.GetString("searchValue") });
            }
            return View(touristPlace);
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var touristPlace = await _context.TouristPlaces
                .FirstOrDefaultAsync(m => m.Id == id);
            //Without Lazy Loading
            /*var touristPlace = await _context.TouristPlaces.Include(m=>m.Type)
                .FirstOrDefaultAsync(m => m.Id == id);*/
            if (touristPlace == null)
            {
                return NotFound();
            }
            ViewBag.Types = _context.PlaceTypes.Where(m => m.Id != touristPlace.TypeId).ToList();
            return View(touristPlace);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, IFormFile Image, [Bind("Id, Name, Rating, Address, TypeId")] TouristPlace touristPlace)
        {
            if (id != touristPlace.Id)
            {
                return NotFound();
            }

            var touristPlaceItem = _context.TouristPlaces.Find(id);

            if (ModelState.IsValid)
            {
                try
                {

                    if (Image != null)
                    {
                        touristPlaceItem.ImageUrl = SaveImageReturnUniqueName(Image);

                    }
                    touristPlaceItem.Name = touristPlace.Name;
                    touristPlaceItem.Rating = touristPlace.Rating;
                    touristPlaceItem.Address = touristPlace.Address;
                    touristPlaceItem.TypeId = touristPlace.TypeId;
                    _context.Update(touristPlaceItem);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TouristPlaceExists(touristPlace.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index", new { searchValue = HttpContext.Session.GetString("searchValue") });

            }
            return View(touristPlace);

        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var touristPlace = await _context.TouristPlaces
                .FirstOrDefaultAsync(m => m.Id == id);

            //Without Lazy Loading
            /*var touristPlace = await _context.TouristPlaces.Include(m=>m.Type)
                .FirstOrDefaultAsync(m => m.Id == id);*/
            if (touristPlace == null)
            {
                return NotFound();
            }

            return View(touristPlace);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var touristPlace = await _context.TouristPlaces.FindAsync(id);
            _context.TouristPlaces.Remove(touristPlace);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { searchValue = HttpContext.Session.GetString("searchValue") });
        }

        private bool TouristPlaceExists(int id)
        {
            return _context.TouristPlaces.Any(e => e.Id == id);
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("UserLogin", "Login");
        }

        public string SaveImageReturnUniqueName(IFormFile Image)
        {
            var uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images");
            string uniqueName = Guid.NewGuid().ToString() + "_" + Image.FileName;
            string fileUrl = Path.Combine(uploadFolder, uniqueName);
            Image.CopyTo(new FileStream(fileUrl, FileMode.Create));
            return uniqueName;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouristPlaceFluentApi.Data;

namespace TouristPlaceFluentApi.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new TouristPlaceContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<TouristPlaceContext>>()))
            {
                if (context.Users.Any())
                {
                    return;
                }

                context.Users.AddRange(
                    new User
                    {
                        FirstName = "Harry",
                        LastName = "Met",
                        UserID = "harry_met",
                        Password = "Harry"
                    },

                    new User
                    {
                        FirstName = "Mahmudul",
                        LastName = "Hasan",
                        UserID = "mahmudul_hasan",
                        Password = "Mahmudul"
                    },

                    new User
                    {
                        FirstName = "Shihab",
                        LastName = "Haider",
                        UserID = "shihab_haider",
                        Password = "Shihab"
                    }

                );
                context.SaveChanges();
            }
        }
    }
}

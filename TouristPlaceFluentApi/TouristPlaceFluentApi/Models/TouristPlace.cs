﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TouristPlaceFluentApi.Models
{
    public class TouristPlace
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        [Required]
        public int Rating { get; set; }

        [Required]
        public string Address { get; set; }
        [Display(Name = "Type")]
        public int TypeId { get; set; }
        public virtual PlaceType Type { get; set; }
    }
}

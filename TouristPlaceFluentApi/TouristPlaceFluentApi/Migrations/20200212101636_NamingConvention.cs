﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TouristPlaceFluentApi.Migrations
{
    public partial class NamingConvention : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_touristPlace_PlaceType_TypeId",
                table: "touristPlace");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserTable",
                table: "UserTable");

            migrationBuilder.DropPrimaryKey(
                name: "PK_touristPlace",
                table: "touristPlace");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlaceType",
                table: "PlaceType");

            migrationBuilder.RenameTable(
                name: "UserTable",
                newName: "Users");

            migrationBuilder.RenameTable(
                name: "touristPlace",
                newName: "TouristPlaces");

            migrationBuilder.RenameTable(
                name: "PlaceType",
                newName: "PlaceTypes");

            migrationBuilder.RenameIndex(
                name: "IX_touristPlace_TypeId",
                table: "TouristPlaces",
                newName: "IX_TouristPlaces_TypeId");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "PlaceTypes",
                newName: "Name");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "PlaceTypes",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "UserID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TouristPlaces",
                table: "TouristPlaces",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlaceTypes",
                table: "PlaceTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TouristPlaces_PlaceTypes_TypeId",
                table: "TouristPlaces",
                column: "TypeId",
                principalTable: "PlaceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TouristPlaces_PlaceTypes_TypeId",
                table: "TouristPlaces");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TouristPlaces",
                table: "TouristPlaces");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PlaceTypes",
                table: "PlaceTypes");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "UserTable");

            migrationBuilder.RenameTable(
                name: "TouristPlaces",
                newName: "touristPlace");

            migrationBuilder.RenameTable(
                name: "PlaceTypes",
                newName: "PlaceType");

            migrationBuilder.RenameIndex(
                name: "IX_TouristPlaces_TypeId",
                table: "touristPlace",
                newName: "IX_touristPlace_TypeId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "PlaceType",
                newName: "name");

            migrationBuilder.AlterColumn<string>(
                name: "name",
                table: "PlaceType",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserTable",
                table: "UserTable",
                column: "UserID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_touristPlace",
                table: "touristPlace",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PlaceType",
                table: "PlaceType",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_touristPlace_PlaceType_TypeId",
                table: "touristPlace",
                column: "TypeId",
                principalTable: "PlaceType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouristPlaceFluentApi.Models;

namespace TouristPlaceFluentApi.EntityConfiguration
{
    public class PlaceTypeEntityConfiguration : IEntityTypeConfiguration<PlaceType>
    {
        public void Configure(EntityTypeBuilder<PlaceType> builder)
        {
            builder.HasKey(m => m.Id);
            builder.Property(m => m.Id).HasColumnType("int").IsRequired();
            builder.Property(m => m.Name).HasColumnType("nvarchar").IsRequired();
        }
    }
}

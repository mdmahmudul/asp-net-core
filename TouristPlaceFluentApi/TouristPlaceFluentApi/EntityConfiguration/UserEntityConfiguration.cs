﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouristPlaceFluentApi.Models;

namespace TouristPlaceFluentApi.EntityConfiguration
{
    public class UserEntityConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(m => m.Id);
            builder.Property(m => m.Id).HasColumnType("int").IsRequired();
            builder.Property(m => m.FirstName).HasColumnType("nvarchar").IsRequired();
            builder.Property(m => m.LastName).HasColumnType("nvarchar").IsRequired();
            builder.Property(m => m.UserID).HasColumnType("nvarchar").IsRequired();
            builder.Property(m => m.Password).HasColumnType("nvarchar").IsRequired();

        }
    }
}

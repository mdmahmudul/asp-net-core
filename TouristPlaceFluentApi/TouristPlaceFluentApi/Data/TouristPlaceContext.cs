﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouristPlaceFluentApi.EntityConfiguration;
using TouristPlaceFluentApi.Models;

namespace TouristPlaceFluentApi.Data
{

    public class TouristPlaceContext : DbContext
    {
        public TouristPlaceContext(DbContextOptions<TouristPlaceContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TouristPlaceEntityConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityConfiguration());
            modelBuilder.ApplyConfiguration(new PlaceTypeEntityConfiguration());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        public DbSet<TouristPlace> TouristPlaces { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<PlaceType> PlaceTypes { get; set; }
    }
}

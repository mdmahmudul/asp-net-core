﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TouristPlaceFluentApi.Models
{
    public class PlaceType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<TouristPlace> TouristPlaces { get; set; }
    }
}

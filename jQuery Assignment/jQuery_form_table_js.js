


var arr = [];
var id = 0, imgC, sortedValue = 0;

$(document).ready(function(){
	/*to view Tourist Place Form*/
	$('#createTouristPlace').click(function(){
		$('#searchItem').css("display","none");
		$('#mainform').css("display","block");
		$('#tableList').css("display","none");
		$('#createTouristPlace').css("display","none");
		$('#addButton').css("display","block");
		$('#cancelButton').css("display","block");
	});
	/*end view Tourist Place Form*/


	/*to add a Tourist Place*/
	$('#addButton').click(function(){
		var searchStr = $('#searchItems').val();
		var tempObj = {name:"",address:"", rating:0, flag:true, id:0,img:null}
		var flag = true;
		if($('#name').val() != ""){
			tempObj.name = $('#name').val();
			$('#name').css("border-color","black");
		}
		else{
			$('#name').css("border-color","red");
			flag = false;
		}
		if($('#address').val() != ""){
			tempObj.address = $('#address').val();
			$('#address').css("border-color","black");
		}
		else{
			$('#address').css("border-color","red");
			flag = false;
		}
		if(imgC != null){
			$('#picture').css("border-color","black");
			tempObj.img = imgC;
			
		}
		else{
			$('#picture').css("border-color","red");
			flag = false;
		}
		if(flag == false)
			return;
		tempObj.rating = $('#rating').val();
		if(tempObj.name.indexOf(searchStr) != -1)
			tempObj.flag = true;
		else
			tempObj.flag = false;
		arr.push(tempObj);
		imgC = null;
		for(var i =0; i<$('#touristForm').length; i++)
			$('#touristForm')[0].reset();
		/*for sorting*/
		displayTouristPlaceTable();
		/*end sorting*/

		$('#searchItem').css("display","block");
		$('#mainform').css("display","none");
		$('#tableList').css("display","block");
		$('#createTouristPlace').css("display","block");
		$('#imagePreview').css("display","none");
	});
	/*end adding a Tourist Place*/


	/*for searching Tourist Place*/
	$('#searchItems').keyup(function(){
		var searchStr = this.value;
		var len = arr.length;
		for(var i = 0; i<len; i++){
		if(arr[i].name.indexOf(searchStr) != -1){
			arr[i].flag = true;
		}
		else
			arr[i].flag = false;
		}
		touristPlaceTablePrint();
	});
	/*end searching Tourist place*/


	/*cancel to return Tourist place table*/
	$('#cancelButton').click(function(){
		$('#searchItem').css("display","block");
		$('#mainform').css("display","none");
		$('#tableList').css("display","block");
		$('#createTouristPlace').css("display","block");
		$('#updateButton').css("display","none");
		if($('#imagePreviewShow'))
			$('#imagePreviewShow').attr("src","");
		if($('#imagePreview'))
			$('#imagePreview').css("display","none");
		imgC = null;
		for(var i = 0; i<$('#touristForm').length; i++)
			$('#touristForm')[i].reset();
		touristPlaceTablePrint();

	});
	/*end cancel button*/
	

	/*update Tourist Place Value*/
	$('#updateButton').click(function(){
		var id = parseInt($('#hiddenValue').val());
		var tempObj = arr.find(obj => obj.id === id) ;
		var flag = true;
		if($('#name').val() != ""){
			tempObj.name = $('#name').val();
			$('#name').css("border-color","black");
		}
		else{
			$('#name').css("border-color","red");
			flag = false;
		}
		if($('#address').val() != ""){
			tempObj.address = $('#address').val();
			$('#address').css("border-color","black");
		}
		else{
			$('#address').css("border-color","red");
			flag = false;
		}
		if(flag == false)
			return;
		tempObj.rating = $('#rating').val();
		if(imgC != null)
			tempObj.img = imgC;
		var searchStr = $('#searchItems').val();
		if(tempObj.name.indexOf(searchStr) != -1)
			tempObj.flag = true;
		else
			tempObj.flag = false;
		imgC = null;
		for(var i =0; i<$('#touristForm').length; i++)
			$('#touristForm')[0].reset();
		/*for sorting*/
		displayTouristPlaceTable();

		$('#searchItem').css("display","block");
		$('#mainform').css("display","none");
		$('#tableList').css("display","block");
		$('#createTouristPlace').css("display","block");
		$('#updateButton').css("display","none");
		$('#imagePreview').css("display","none");
	});
	/*end update*/


	/*validation*/
	$('.hasValue').keyup(function(){
		if($('#name').val() != "")
			$('#name').css("borderColor", "black");
		if($('#address').val() !="")
			$('#address').css("borderColor", "black");
	});
	/*end validation*/


	/*picture load*/
	$('#picture').change(function(){
		$('#imagePreview').css("display","block");
		const file = $('#picture')[0].files[0];
		const reader = new FileReader();
		var previewImg = $('#imagePreviewShow');
		reader.addEventListener("load", function () {
			    // convert image file to base64 string
			$('#picture').css("border-color","black");
			previewImg.attr("src",reader.result);
			imgC =  reader.result;
		}, false);

		if (file) {
			reader.readAsDataURL(file);
		}
	});
	/*end picture load*/


	/*for sorting order change with rating*/
    $(document).on( "click", "#ratingId", function() {
	    if(sortedValue == 0){
			sortedValue = 1;
		}
		else{
			sortedValue = 0;
		}
		displayTouristPlaceTable();
    });
    /*end sorting order*/


    /*for deleteing an item from Tourist Place Table*/
    $(document).on("click", ".deleteValueId",function(){
    	if(confirm("Do you want to delete")){
			var len = arr.length;
			id = parseInt($(this).attr("rowId"));
			for(var i = 0; i<len; i++){
				if(arr[i].id === id){
					arr.splice(i,1);
					break;
				}
			}
		}
		touristPlaceTablePrint();
    });
    /*end deleting an item from Tourist Place Table*/


    /*for showing update form with specific value*/
    $(document).on("click", ".updateValueClass", function(){
    	var id = parseInt($(this).attr("rowId"));
		var obj = arr.find(obj => obj.id === id) ;
		$('#name').val(obj.name);
		$('#address').val(obj.address);
		$('#rating').val(obj.rating);
		$('#hiddenValue').val(id);
		$('#imagePreviewShow').attr("src",obj.img);

		$('#searchItem').css("display","none");
		$('#mainform').css("display","block");
		$('#tableList').css("display","none");
		$('#createTouristPlace').css("display","none");
		$('#addButton').css("display","none");
		$('#updateButton').css("display","block");
		if($('#imagePreview').css("display") == "none")
			$('#imagePreview').css("display", "block");
    });
    /*end showing update form value*/


	/*for displaying Tourist Place Table*/
	function displayTouristPlaceTable(){
		arr.sort(compare);
		touristPlaceTablePrint();
	}
	/*end displaying*/


	/*for comparing sort*/
	function compare(a, b) {
	    return sortedValue == 0?((a.rating<=b.rating)?1:-1):((a.rating>=b.rating)?1:-1);
	}
	/*end comparing*/


	/*for creating Tourist Place Table from array*/
	function touristPlaceTablePrint(){
		var addTable = $('#tableContent').empty();
		/*for table heading*/
		var tblrow = "<tr>";
		tblrow += "<th>Name</th>";
		tblrow +="<th>Address</th>";
		if(sortedValue == 0){
			tblrow +="<th id='ratingId'>Rating &uarr;</th>";
		}
		else{
			tblrow +="<th id='ratingId'>Rating &darr;</th>";
		}
		tblrow +="<th>Picture</th>";
		tblrow += "<th>Action</th>";
		tblrow += "</tr>";
		/*end table heading*/
		var len = arr.length;
		for(var i =0; i<len; i++){
			arr[i].id = i;
			if(arr[i].flag == false)
				continue;
			tblrow += "<tr>"
			tblrow += "<td>" + arr[i].name + "</td>";
			tblrow += "<td>" + arr[i].address + "</td>";
			tblrow += "<td>" + arr[i].rating + "</td>";
			tblrow += '<td><img src='+arr[i].img+' style="width:150px; height:100px"></td>';
			tblrow += '<td><form><input rowId='+arr[i].id+' type="button" class="updateValueClass button update" value="Update"><input rowId='+arr[i].id+' type="button" class="deleteValueId button delete" value="Delete" ></form></td>';
			tblrow += "</tr>";
		}
		addTable.append(tblrow);
	}
	/*end creating Tourist Place Table*/

});

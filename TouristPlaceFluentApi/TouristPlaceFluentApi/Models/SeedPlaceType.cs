﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TouristPlaceFluentApi.Data;

namespace TouristPlaceFluentApi.Models
{
    public static class SeedPlaceType
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new TouristPlaceContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<TouristPlaceContext>>()))
            {
                if (context.PlaceTypes.Any())
                {
                    return;  
                }

                context.PlaceTypes.AddRange(
                    new PlaceType
                    {
                        Name  = "Mountain"
                    },

                    new PlaceType
                    {
                        Name = "River",
                    },

                    new PlaceType
                    {
                        Name = "Park",
                        
                    }

                );
                context.SaveChanges();
            }
        }
    }
}
